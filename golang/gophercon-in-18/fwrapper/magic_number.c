#include "magic_number.h"

int find_magic_number(int number) {
	return 10 * number;
}

YoFunc get_magic_func() {
	return find_magic_number;
}

// Wrapper function
int get_magic_number(YoFunc func, int number) {
	return func(number);
}