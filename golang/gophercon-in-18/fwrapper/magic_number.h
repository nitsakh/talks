
#ifndef MAGIC_NUMBER
#define MAGIC_NUMBER

typedef int (*YoFunc) (int);

int find_magic_number(int number);

YoFunc get_magic_func();

int get_magic_number(YoFunc func, int number);

#endif // MAGIC_NUMBER