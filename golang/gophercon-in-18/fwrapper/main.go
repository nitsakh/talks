package main

/*
#include "magic_number.h"
*/
import "C"

import (
	"fmt"
	"strconv"
)

func main() {
	fptr := C.get_magic_func()
	str := strconv.Itoa(int(C.get_magic_number(fptr, 10)))
	fmt.Println("Magic Number is: " + str)
}
