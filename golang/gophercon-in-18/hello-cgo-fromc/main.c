#include <stdio.h>
#include <stdlib.h>

#include "libGetString/libGetString.h"

int main(int argc, char* argv[]) {
    char* w = GetString();
    printf("HELLO %s", w);
    free(w);
    return 0;
}