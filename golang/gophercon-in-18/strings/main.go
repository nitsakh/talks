package main

/*
#include <string.h>

char* GetString() {
	return "WoRlD!";
}
*/
import "C"
import (
	"fmt"
	"unsafe"
)

// GetStringWithGoString uses C.GoString to copy C String to Go
func GetStringWithGoString() {
	cstr := C.GetString()
	fmt.Println("Hello " + C.GoString(cstr))
}

// GetStringWithoutGoString reads the C string and copies it into Go slice
func GetStringWithoutGoString() {
	cstr := C.GetString()
	cstrlen := C.strlen(cstr)
	slc := make([]byte, cstrlen)
	cBuf := (*[1 << 30]byte)(unsafe.Pointer(cstr))[:int(cstrlen):int(cstrlen)]
	copy(slc, cBuf)
	fmt.Println("Hii " + string(cBuf))
}

func main() {
	GetStringWithGoString()
	GetStringWithoutGoString()
}
