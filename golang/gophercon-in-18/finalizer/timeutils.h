
#ifndef TIME_UTILS
#define TIME_UTILS

#include <time.h>

time_t* CreateTime();

char* GetTime(time_t* t);

void FreeTime(time_t* time);

#endif // TIME_UTILS