#include "timeutils.h"

#include <stdlib.h>

time_t* CreateTime() {
	return malloc(sizeof(time_t));
}

char* GetTime(time_t* t) {
	time_t mytime = time(t);
	return ctime(&mytime);
}

void FreeTime(time_t* time) {
	free(time);
}