package main

/*
#include "timeutils.h"
*/
import "C"
import (
	"fmt"
	"runtime"
	"time"
)

type NiceTime struct {
	cstr *C.time_t
}

func NewNiceTime() *NiceTime {
	fmt.Println("Allocating...")
	t := &NiceTime{C.CreateTime()}
	runtime.SetFinalizer(t, Free)
	return t
}

func Free(s *NiceTime) {
	fmt.Println("Freeing...")
	C.FreeTime(s.cstr)
}

func main() {
	t := NewNiceTime()
	fmt.Println(C.GoString(C.GetTime(t.cstr)))
	runtime.GC()
	time.Sleep(time.Second)
}
