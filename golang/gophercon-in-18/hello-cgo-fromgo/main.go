package main

/*
char* GetString() {
	return "WORlD!";
}
*/
import "C"
import (
	"fmt"
)

func GetString() string {
	return "WORlD!"
}

func main() {
	cstr := C.GetString()
	fmt.Println("hello " + C.GoString(cstr))
}
