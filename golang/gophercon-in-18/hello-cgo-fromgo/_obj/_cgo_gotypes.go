// Created by cgo - DO NOT EDIT

package main

import "unsafe"

import _ "runtime/cgo"

import "syscall"

var _ syscall.Errno
func _Cgo_ptr(ptr unsafe.Pointer) unsafe.Pointer { return ptr }

//go:linkname _Cgo_always_false runtime.cgoAlwaysFalse
var _Cgo_always_false bool
//go:linkname _Cgo_use runtime.cgoUse
func _Cgo_use(interface{})
type _Ctype_char int8

type _Ctype_void [0]byte

//go:linkname _cgo_runtime_cgocall runtime.cgocall
func _cgo_runtime_cgocall(unsafe.Pointer, uintptr) int32

//go:linkname _cgo_runtime_cgocallback runtime.cgocallback
func _cgo_runtime_cgocallback(unsafe.Pointer, unsafe.Pointer, uintptr, uintptr)

//go:linkname _cgoCheckPointer runtime.cgoCheckPointer
func _cgoCheckPointer(interface{}, ...interface{})

//go:linkname _cgoCheckResult runtime.cgoCheckResult
func _cgoCheckResult(interface{})

//go:cgo_import_static _cgo_725864bf54a8_Cfunc_GetString
//go:linkname __cgofn__cgo_725864bf54a8_Cfunc_GetString _cgo_725864bf54a8_Cfunc_GetString
var __cgofn__cgo_725864bf54a8_Cfunc_GetString byte
var _cgo_725864bf54a8_Cfunc_GetString = unsafe.Pointer(&__cgofn__cgo_725864bf54a8_Cfunc_GetString)

//go:cgo_unsafe_args
func _Cfunc_GetString() (r1 *_Ctype_char) {
	_cgo_runtime_cgocall(_cgo_725864bf54a8_Cfunc_GetString, uintptr(unsafe.Pointer(&r1)))
	if _Cgo_always_false {
	}
	return
}

//go:linkname _cgo_runtime_gostring runtime.gostring
func _cgo_runtime_gostring(*_Ctype_char) string

func _Cfunc_GoString(p *_Ctype_char) string {
	return _cgo_runtime_gostring(p)
}
