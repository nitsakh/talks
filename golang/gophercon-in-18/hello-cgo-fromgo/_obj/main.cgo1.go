// Created by cgo - DO NOT EDIT

//line main.go:1
package main

/*
char* GetString() {
	return "WORlD!";
}
*/
import _ "unsafe"
import (
	"fmt"
)

func GetString() string {
	return "WORlD!"
}

func main() {
	cstr := (_Cfunc_GetString)()
	fmt.Println("hello " + (_Cfunc_GoString)(cstr))
}
