package main

/*
char* GetString() {
	return "WORlD!";
}
*/
import "C"

func GetStringGo() string {
	return "WORlD!"
}

func GetStringC() *C.char {
	return C.GetString()
}

func main() {}
