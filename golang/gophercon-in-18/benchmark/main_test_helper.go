package main

import "testing"

func benchGetStringGo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = GetStringGo()
	}
}

func benchGetStringC(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = GetStringC()
	}
}
