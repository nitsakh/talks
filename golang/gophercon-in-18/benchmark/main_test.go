package main

import "testing"

// Run these benchmarks with `go test -bench . -gcflags '-l'`

func BenchmarkGoGetString(b *testing.B) {
	benchGetStringGo(b)
}

func BenchmarkCGetString(b *testing.B) {
	benchGetStringC(b)
}
